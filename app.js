const express = require('express');
const app = express();
const port = 3000;
const db = require('./db');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello :)'));

app.get('/todos', db.getItems);
app.post('/todo', db.setItem);
app.delete('/todo', db.deleteItem);
app.put('/todo', db.insertItem);

app.listen(port, () => console.log(`Server started on port ${port}!`));

export default app;
