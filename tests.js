import chai from 'chai';
import chaiHttp from 'chai-http';
import app from './app';

chai.use(chaiHttp);
chai.should();

describe('Todos', () => {
  describe('GET /todos', () => {
    it('should get all todos record', (done) => {
      chai.request(app)
        .get('/todos')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('items');
          done();
        })
    });
  });

  describe('POST /todo', () => {
    const id = 2;
    it(`should set #${id} todo\'s completed value`, (done) => {
      const completed = 1;
      chai.request(app)
        .post('/todo')
        .send({id, completed})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          done();
        });
    });
    it(`should return 404 status`, (done) => {
      chai.request(app)
        .post('/todo')
        .send({id: 0, completed: 'Something'})
        .end((err, res) => {
          res.should.have.status(404);
          res.body.status.should.equal('not_found');
          done();
        });
    });
  });

  describe('DELETE /todo', () => {
    const id = 4;
    it(`should delete #${id} record`, (done) => {
      chai.request(app)
        .delete('/todo')
        .send({id})
        .end((err, res) => {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          done();
        });
    });
  });

  describe('PUT /todo', () => {
    const value = 'New record by test';
    it('should insert new record', (done) => {
      chai.request(app)
        .put('/todo')
        .send({value})
        .end((err, res) => {
          res.should.have.status(201);
          res.body.status.should.equal('success');
          res.body.should.have.a('object');
          done();
        });
    });
  });
});
