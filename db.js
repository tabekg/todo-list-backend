const mysql = require('mysql')
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'todos'
})
db.connect()

module.exports = {
	getItems: (req, res) => {
    db.query('SELECT * FROM `todos`', (err, items) => {
      if (err) throw err;
      else res.status(200).json({items});
    });
	},
	setItem: (req, res) => {
    const {id, completed} = req.body;
    db.query('SELECT * FROM `todos` WHERE `id`=? LIMIT 1', [id], (err, rows) => {
      if (err) throw err;
      if (rows.length > 0){
        db.query('UPDATE `todos` SET `completed`=? WHERE `id`=? LIMIT 1', [completed == true ? 1 : 0, id], (_err, response) => {
          if (_err) throw _err;
          res.status(200).json({status: 'success'});
        });
      }	else res.status(404).json({status: 'not_found'});
    });
	},
	deleteItem: (req, res) => {
    const {id} = req.body;
    db.query('DELETE FROM `todos` WHERE `id`=? LIMIT 1', [id], (err) => {
      if (err) throw err;
      else res.status(200).json({status: 'success'});
    });
	},
	insertItem: (req, res) => {
    const {value} = req.body;
    if (value.length > 0){
      db.query('INSERT INTO `todos` SET `value`=?, `completed`=0', [value], (err, response) => {
        if (err) throw err;
        else res.status(201).json({
          item: {
            id: response.insertId,
            value
          },
          status: 'success'
        });
      });
    } else  {
      res.status(400).json({status: 'invalid_params'});
    }
	}
};
